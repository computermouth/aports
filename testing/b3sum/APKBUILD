# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer:
pkgname=b3sum
pkgver=1.1.0
pkgrel=0
pkgdesc="Command line implementation of the BLAKE3 hash function"
url="https://blake3.io"
# armhf: 'vaddq_u32': target specific option mismatch
arch="all !s390x !armhf !mips !mips64 !riscv64"
license="CC0-1.0 OR Apache-2.0"
makedepends="cargo"
source="b3sum-$pkgver.tar.gz::https://crates.io/api/v1/crates/b3sum/$pkgver/download"

case "$CARCH" in
	arm*) _features="neon" ;;
esac

build() {
	cargo build --release --locked --no-default-features ${_features:+--features="$_features"}
}

check() {
	cargo test --release --locked --no-default-features ${_features:+--features="$_features"}
}

package() {
	cargo install --locked --path . --root="$pkgdir/usr" --no-default-features ${_features:+--features "$_features"}
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
a163230d8f73a531612c5217a2fd82831dc251b67d9b2a7318d286bb4bd9673d5ad03c841a8f4f168b82e9c7d9a655d0a90671910d88ed2ba55bfeba5a8f3007  b3sum-1.1.0.tar.gz
"
