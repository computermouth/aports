# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland-server
pkgver=5.23.2
pkgrel=0
pkgdesc="Wayland Server Components built on KDE Frameworks"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only"
depends_dev="
	kwayland-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	linux-headers
	qt5-qttools-dev
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-server-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-dbg"
options="!check" # Requires running wayland compositor

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
16f79d47c95357041e1605bdcde4e4c24f54592ff8d2b273389706ef2ec1a313ede20b6e05f64fabca2d35b338adeb4446bebab1a4a95a940f37be56277ef703  kwayland-server-5.23.2.tar.xz
"
