# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ruby-rake
_gemname=rake
# Keep version in sync with "Bundled gems" (https://stdgems.org) for the
# packaged Ruby version.
pkgver=13.0.3
pkgrel=0
pkgdesc="A Ruby task runner, inspired by make"
url="https://github.com/ruby/rake"
arch="noarch"
license="MIT"
depends="ruby"
checkdepends="ruby-minitest"
makedepends="ruby-rdoc"
subpackages="$pkgname-doc"
source="https://github.com/ruby/rake/archive/v$pkgver/$_gemname-$pkgver.tar.gz
	gemspec.patch
	"
builddir="$srcdir/$_gemname-$pkgver"

build() {
	gem build $_gemname.gemspec
}

check() {
	# FIXME: Fix test_signal_propagation_in_tests
	ruby -Ilib -Itest -e "Dir.glob('./test/**/test_*.rb', &method(:require))" -- \
		--exclude=test_signal_propagation_in_tests
}

package() {
	local gemdir="$pkgdir/$(ruby -e 'puts Gem.default_dir')"

	gem install \
		--local \
		--install-dir "$gemdir" \
		--bindir "$pkgdir/usr/bin" \
		--ignore-dependencies \
		--document ri \
		--verbose \
		$_gemname

	# Remove unnessecary files
	cd "$gemdir"
	rm -rf build_info cache extensions plugins
}

doc() {
	pkgdesc="$pkgdesc (ri docs)"

	amove "$(ruby -e 'puts Gem.default_dir')"/doc
}

sha512sums="
aff09bbfa58620cd6902e6679ae3f54d90424bfac40c7ad6efcc590a1c9624ae7a0c5597b8b05b378d90e422285c41d781485b9aebe1819c0c2eaf5f2624afa2  rake-13.0.3.tar.gz
f96cf62c017962145e6553534c4c1e0ae678821781e52b3000ea21ea908a010fc1a25d505d1b0f7262db70b1354a790a1ef3f2f9e59bba23bd701beda69686d2  gemspec.patch
"
